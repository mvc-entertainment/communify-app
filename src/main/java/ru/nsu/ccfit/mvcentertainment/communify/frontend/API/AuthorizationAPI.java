package ru.nsu.ccfit.mvcentertainment.communify.frontend.API;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.Token;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.User;

public interface AuthorizationAPI {
    @POST("sign-up")
    Call<User> signUp(@Body User user);

    @POST("sign-in")
    Call<Token> signIn(@Body User user);
}
