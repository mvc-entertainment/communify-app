package ru.nsu.ccfit.mvcentertainment.communify.frontend.API;

import com.google.gson.JsonElement;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.*;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.*;

public interface PlaylistAPI {
    @GET("playlists/{id}/tracks?size=1000000")
    Call<TracksDTO> getPlaylistTracks(@Header("Authorization") String token, @Path("id") long id, @Query("page") int page);

    @PUT("playlists/{id}/cover")
    @Headers("Content-Type: application/octet-stream")
    Call<JsonElement> setPlaylistCover(@Header("Authorization") String token, @Path("id") long id, @Body RequestBody cover);

    @POST("playlists/{id}/tracks")
    @Multipart
    Call<JsonElement> uploadTrackToPlaylists(@Header("Authorization") String token, @Path("id") long id,
                                             @Part("trackInfo") Track trackInfo, @Part MultipartBody.Part audioFile);

    @PUT("playlists/{id}")
    Call<Playlist> updatePlaylistInfo(@Header("Authorization") String token, @Path("id") long id, @Body Playlist playlist);

    @GET("playlists/{id}")
    Call<Playlist> getPlaylist(@Header("Authorization") String token, @Path("id") long id);

    @DELETE("playlists/{id}/tracks/{trackId}")
    Call<Track> deleteTrackFromPlaylist(@Header("Authorization") String token, @Path("id") long id, @Path("trackId") long trackId);

    @POST("playlists")
    Call<Playlist> createPlaylist(@Header("Authorization") String token, @Body Playlist playlist);

    @POST("playlists/search")
    Call<PlaylistsDTO> search(@Header("Authorization") String token, @Body PlaylistFilter filter);
}
