package ru.nsu.ccfit.mvcentertainment.communify.frontend.API;

import com.google.gson.JsonElement;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.PlaylistsDTO;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.User;

public interface UserAPI {
    @GET("users/{id}")
    Call<User> getUser(@Header("Authorization") String token, @Path("id") long id);

    @GET("users/{id}/playlists?size=1000000")
    Call<PlaylistsDTO> getUserPlaylists(@Header("Authorization") String token, @Path("id") long id, @Query("page") int page);

    @GET("users/{id}/owned-playlists?size=1000000")
    Call<PlaylistsDTO> getOwnedPlaylists(@Header("Authorization") String token, @Path("id") long id, @Query("page") int page);

    @PUT("users/{userId}/playlists/{playlistId}")
    Call<User> addPlaylist(@Header("Authorization") String token, @Path("userId") long userId, @Path("playlistId") long playlistId);

    @DELETE("users/{userId}/playlists/{playlistId}")
    Call<User> deletePlaylist(@Header("Authorization") String token, @Path("userId") long userId, @Path("playlistId") long playlistId);

    @PUT("users/{id}/icon")
    @Headers("Content-Type: application/octet-stream")
    Call<JsonElement> setUserIcon(@Header("Authorization") String token, @Path("id") long id, @Body RequestBody icon);

    @GET("users/{id}/icon")
    Call<ResponseBody> getUserIcon(@Header("Authorization") String token, @Path("id") long id);

    @PUT("users/{id}")
    Call<User> updateUser(@Header("Authorization") String token, @Path("id") long id, @Body User info);
}
