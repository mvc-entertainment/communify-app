package ru.nsu.ccfit.mvcentertainment.communify.frontend;

import javafx.beans.value.ChangeListener;
import javafx.util.Duration;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.Playlist;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.Track;

public interface AudioPlayer {
    void play();
    void pause();
    void setVolume(double volume);
    double getVolume();
    void seek(double duration);
    double getTotalSeconds();
    double getCurrentSeconds();
    double getCurrentTime();
    void setPlaylist(Playlist playlist);
    Playlist getPlaylist();
    void nextTrack();
    void previousTrack();
    void setTrack(int index);
    int getTrack();
    void addTimeListener(ChangeListener<Duration> listener);
    void addOnTrackStart(Runnable runnable);
    void addOnPlaylistChange(Runnable runnable);
    Track getCurrentTrack();
    boolean isPlaying();
}
