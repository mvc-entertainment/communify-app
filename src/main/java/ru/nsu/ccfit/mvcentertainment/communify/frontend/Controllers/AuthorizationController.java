package ru.nsu.ccfit.mvcentertainment.communify.frontend.Controllers;

import com.google.gson.JsonElement;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import retrofit2.Response;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.API.AuthorizationAPI;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.Token;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.User;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.GUI.Main;

import java.io.IOException;

public class AuthorizationController {
    //registration
    @FXML private TextField registrationName;
    @FXML private PasswordField registrationPassword;
    @FXML private PasswordField registrationPasswordCheck;
    @FXML private Button registrationSubmit;
    //sign in
    @FXML private TextField signInName;
    @FXML private PasswordField signInPassword;
    @FXML private Button signInSubmit;

    private AuthorizationAPI authorizationAPI;

    public void init(Stage stage) {
        authorizationAPI = Main.getRetrofit().create(AuthorizationAPI.class);
        setupRegistration(stage);
        setupSignIn(stage);
    }

    private void setupRegistration(Stage stage) {
        registrationSubmit.setOnAction(actionEvent -> {
            if(registrationPassword.getText().equals(registrationPasswordCheck.getText())) {
                if(registrationName.getText().equals("")) {
                    Main.error("Укажите имя пользователя.");
                    return;
                }
                Main.getHttpRequestsExecutor().execute(() -> {
                    setup(stage, registrationName, registrationPassword, registrationSubmit, true);
                });
            }
            else {
                Main.error("Пароли не совпадают.");
            }
        });
    }

    private void setupSignIn(Stage stage) {
        signInSubmit.setOnAction(actionEvent -> {
            Main.getHttpRequestsExecutor().execute(() -> {
                setup(stage, signInName, signInPassword, signInSubmit, false);
            });
        });
    }

    private void setup(Stage stage, TextField nameField, PasswordField passwordField, Button button, boolean isSignUp) {
        try {
            button.setDisable(true);
            User user = new User(nameField.getText(), passwordField.getText(), "", 0);
            if(isSignUp) {
                authorizationAPI.signUp(user).execute();
            }
            Response<Token> signIn = authorizationAPI.signIn(user).execute();
            Main.setToken(signIn.body().getToken());
            if(signIn.isSuccessful()) {
                Platform.runLater(() -> {
                    MainController.getMainController().updateUser();
                    MainController.getMainController().update();
                    stage.close();
                });
            }
            else {
                Main.error("Произошла ошибка." + signIn.message());
                button.setDisable(false);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
