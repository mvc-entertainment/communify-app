package ru.nsu.ccfit.mvcentertainment.communify.frontend.Controllers;

import com.google.gson.JsonParser;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import lombok.Getter;
import lombok.Setter;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.API.PlaylistAPI;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.API.UserAPI;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.AudioPlayer;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.PlaylistFilter;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.User;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.GUI.Main;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.GUI.PlaylistUIFactory;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.Playlist;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.Track;

import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.List;

@Getter
public class MainController {
    @Getter private static MainController mainController;

    @FXML private ListView<Track> currentPlaylist;
    @FXML private AnchorPane playlistPane;
    @FXML private Label playlistName;
    @FXML private Button playlistInfoButton;
    @FXML private Button addPlaylistButton;
    @FXML private Slider volumeSlider;
    @FXML private Slider timeSlider;
    @FXML private Label currentTimeLabel;
    @FXML private Label endTimeLabel;
    @FXML private ImageView pauseButton;
    @FXML private Label trackName;
    @FXML private Label trackBand;
    @FXML private ImageView nextButton;
    @FXML private ImageView prevButton;
    @FXML private Button signOutButton;
    @FXML private Label userName;
    @FXML private Button createPlaylist;
    @FXML private TextField searchField;
    @FXML private Label playlistPaneLabel;
    @FXML private Label chosenUserInfoLabel;
    @FXML private Button userPlaylistsButton;

    private Image playImage, pauseImage, nextImage, prevImage;

    private AudioPlayer audioPlayer;

    private int playlistCount = 0;

    private boolean canBeSlided = false;

    private UserAPI userAPI;
    private PlaylistAPI playlistAPI;
    private User user;
    private List<Playlist> loadedPlaylists;
    private List<Playlist> userPlaylists;
    private Playlist chosenPlaylist;
    @Setter private User chosenUser;

    public MainController() {
        mainController = this;
    }

    public void init(AudioPlayer audioPlayer) {
        this.audioPlayer = audioPlayer;

        userAPI = Main.getRetrofit().create(UserAPI.class);
        playlistAPI = Main.getRetrofit().create(PlaylistAPI.class);

        setupPlayerPane();
        setupPlaylistPane();
        setupAudioPlayer();
        setupSliders();

        updateUser();
        update();

        signOutButton.setOnAction(actionEvent -> {
            signOut();
        });
        playlistInfoButton.setOnAction(actionEvent -> {
            showPlaylistInfo();
        });
        createPlaylist.setOnAction(actionEvent -> {
            showPlaylistCreation();
        });
        searchField.setOnAction(actionEvent -> {
            searchField.setDisable(true);
            Main.getHttpRequestsExecutor().execute(() -> {
                update();
                searchField.setDisable(false);
            });
        });
        userPlaylistsButton.setOnAction(actionEvent -> {
            chosenUser = user;
            userPlaylistsButton.setDisable(true);
            searchField.setText("");
            Main.getHttpRequestsExecutor().execute(() -> {
                update();
                Platform.runLater(() -> {
                    userPlaylistsButton.setDisable(false);
                });
            });
        });
        setUserLabel(chosenUserInfoLabel);
        setUserLabel(userName);

        chosenUserInfoLabel.setOnMouseClicked(mouseEvent -> {
            showUserInfo(chosenUserInfoLabel, chosenUser);
        });
        userName.setOnMouseClicked(mouseEvent -> {
            showUserInfo(userName, user);
        });
    }

    public static void setUserLabel(Label label) {
        label.setOnMouseEntered(mouseEvent -> {
            label.setTextFill(Color.LIGHTBLUE);
            label.setUnderline(true);
        });
        label.setOnMouseExited(mouseEvent -> {
            label.setTextFill(Color.WHITE);
            label.setUnderline(false);
        });
    }

    public void updateUser() {
        userName.setText("");
        try {
            user = userAPI.getUser(getToken(), getCurrentUserId()).execute().body();
            chosenUser = user;
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(user != null) {
            Platform.runLater(() -> {
                userName.setText(user.getName());
            });
        }
    }

    public void update() {
        if(chosenUser != null) {
            loadUserPlaylists(chosenUser.getId(), searchField.getText().equals(""));
        }
        else {
            return;
        }
        if(!searchField.getText().equals("")) {
            findPlaylists(true);
            Platform.runLater(() -> {
                playlistPaneLabel.setText("Плейлисты по запросу \"" + searchField.getText() + "\"");
            });
            return;
        }
        if(chosenUser.getId() == user.getId()) {
            Platform.runLater(() -> {
                playlistPaneLabel.setText("Ваши плейлисты");
                chosenUserInfoLabel.setVisible(false);
            });
            return;
        }
        Platform.runLater(() -> {
            playlistPaneLabel.setText("Плейлисты " + chosenUser.getName());
            chosenUserInfoLabel.setVisible(true);
            chosenUserInfoLabel.setText("О пользователе " + chosenUser.getName());
        });
    }

    public void signOut() {
        Main.setToken(null);
        audioPlayer.pause();
        try {
            clearPlaylistUI();
            Main.createAuthorizationWindow();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Stage showUserInfo(Node node, User user) {
        try {
            if(node != null) {
                node.setDisable(true);
            }
            Stage stage = new Stage();
            FXMLLoader loader = new FXMLLoader(PlaylistInfoController.class.getClassLoader().getResource("userInfo.fxml"));
            stage.setScene(new Scene(loader.load()));
            ((UserInfoController)loader.getController()).init(user);

            stage.setOnCloseRequest(windowEvent -> {
                if(node != null) {
                    node.setDisable(false);
                }
            });
            stage.setResizable(false);
            stage.setTitle("Информация о " + user.getName());
            stage.show();
            return stage;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void showPlaylistCreation() {
        try {
            Stage stage = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("playlistCreate.fxml"));
            Scene scene = new Scene(loader.load());
            ((PlaylistCreateController)loader.getController()).init(stage);

            createPlaylist.setDisable(true);
            stage.setScene(scene);
            stage.setResizable(false);
            stage.setTitle("Создание плейлиста");
            stage.setOnCloseRequest(windowEvent -> {
                createPlaylist.setDisable(false);
            });
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showPlaylistInfo() {
        try {
            Stage stage = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("playlistInfo.fxml"));
            Scene scene = new Scene(loader.load());
            ((PlaylistInfoController)loader.getController()).init(chosenPlaylist);

            playlistInfoButton.setDisable(true);
            stage.setScene(scene);
            stage.setResizable(false);
            stage.setTitle("Информация о \"" + chosenPlaylist.toString() + "\"");
            stage.setOnCloseRequest(windowEvent -> {
                playlistInfoButton.setDisable(false);
                if(((PlaylistInfoController)loader.getController()).getTracksEdit() != null) {
                    Window window = ((PlaylistInfoController)loader.getController()).getTracksEdit().getScene().getWindow();
                    window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
                }
            });
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void findPlaylists(boolean addUI) {
        try {
            PlaylistFilter filter = new PlaylistFilter(searchField.getText());
            loadedPlaylists = playlistAPI.search(getToken(), filter).execute().body().getContent();
            if(addUI) {
                Platform.runLater(this::addPlaylistsUI);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadUserPlaylists(long userId, boolean addUI) {
        try {
            loadedPlaylists = userAPI.getUserPlaylists(getToken(), userId, 0).execute().body().getContent();
            userPlaylists = loadedPlaylists;
            if(addUI) {
                Platform.runLater(this::addPlaylistsUI);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addPlaylistsUI() {
        clearPlaylistUI();
        for(Playlist playlist : loadedPlaylists) {
            updatePlaylist(playlist, false, () -> getMainController().addPlaylistUI(playlist));
        }
    }

    public static String getToken() {
        return "Bearer " + Main.getToken();
    }

    public void updatePlaylist(Playlist playlist, boolean addTracks, Runnable after) {
        playlist.setImageCover(Main.getConfiguration().getProperty("host.url") + "playlists/" + playlist.getId() + "/cover");
        if(addTracks) {
            setPlaylistTracks(playlist);
        }
        Platform.runLater(after);
    }

    private void setPlaylistTracks(Playlist playlist) {
        try {
            playlist.setTracks(Playlist.playlistAPI.getPlaylistTracks(getToken(), playlist.getId(), 0).execute().body().getContent());
            for (Track track : playlist.getTracks()) {
                track.setMedia(Main.getConfiguration().getProperty("host.url") + "tracks/" + track.getId());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private long getCurrentUserId() {
        if(Main.getToken() == null) {
            return -1;
        }
        Base64.Decoder decoder = Base64.getUrlDecoder();
        String[] parts = Main.getToken().split("\\.");
        String json = new String(decoder.decode(parts[1]));
        return Long.parseLong(JsonParser.parseString(json).getAsJsonObject().get("userId").toString());
    }

    private void setupSliders() {
        volumeSlider.setValue(volumeSlider.getMax());
        volumeSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            audioPlayer.setVolume(volumeSlider.getValue() / volumeSlider.getMax());
        });
        timeSlider.setOnMousePressed(event -> {
            canBeSlided = true;
        });
        timeSlider.setOnMouseReleased(event -> {
            canBeSlided = false;
        });
        timeSlider.valueProperty().addListener(((observable, oldValue, newValue) -> {
            if(canBeSlided) {
                audioPlayer.seek(newValue.doubleValue() / timeSlider.getMax());
            }
        }));
    }

    private void setupAudioPlayer() {
        audioPlayer.addTimeListener(((observable, oldValue, newValue) -> {
            if(!canBeSlided) {
                timeSlider.setValue(audioPlayer.getCurrentTime() * timeSlider.getMax());
            }
            int currentSeconds = (int)audioPlayer.getCurrentSeconds();
            currentTimeLabel.setText(secondToString(currentSeconds));
        }));
        audioPlayer.addOnTrackStart(() -> {
            int endSeconds = (int)audioPlayer.getTotalSeconds();
            endTimeLabel.setText(secondToString(endSeconds));
            trackName.setText(audioPlayer.getCurrentTrack().getName());
            trackBand.setText(audioPlayer.getCurrentTrack().getAuthor());
            currentPlaylist.refresh();
            if(!audioPlayer.isPlaying()) {
                pauseButton.setImage(playImage);
            }
            else {
                pauseButton.setImage(pauseImage);
            }
        });
    }

    public void showPlaylist(Playlist playlist) {
        currentPlaylist.getItems().clear();
        try {
            playlistName.setText(playlist.toString());
            for (Track t : playlist.getTracks()) {
                currentPlaylist.getItems().add(t);
            }
        }
        catch(NullPointerException npe) {
            playlistName.setText("");
        }
    }

    private void setupPlayerPane() {
        playImage = new Image(PlaylistInfoController.class.getClassLoader().getResource("img/play.png").toString());
        pauseImage = new Image(PlaylistInfoController.class.getClassLoader().getResource("img/pause.png").toString());
        nextImage = new Image(PlaylistInfoController.class.getClassLoader().getResource("img/next.png").toString());
        prevImage = new Image(PlaylistInfoController.class.getClassLoader().getResource("img/prev.png").toString());

        pauseButton.setImage(playImage);
        pauseButton.setOnMouseClicked(mouseEvent -> {
            if(audioPlayer.isPlaying()) {
                audioPlayer.pause();
                pauseButton.setImage(playImage);
            }
            else {
                audioPlayer.play();
                pauseButton.setImage(pauseImage);
            }
        });
        nextButton.setImage(nextImage);
        nextButton.setOnMouseClicked(mouseEvent -> {
            audioPlayer.nextTrack();
        });
        prevButton.setImage(prevImage);
        prevButton.setOnMouseClicked(mouseEvent -> {
            audioPlayer.previousTrack();
        });
    }

    private void setupPlaylistPane() {
        currentPlaylist.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        currentPlaylist.setOnMouseClicked(mouseEvent -> {
            if(mouseEvent.getButton().equals(MouseButton.PRIMARY) && mouseEvent.getClickCount() == 2) {
                if(audioPlayer.getPlaylist() == null || audioPlayer.getPlaylist().getId() != chosenPlaylist.getId()) {
                    audioPlayer.setPlaylist(chosenPlaylist);
                }
                audioPlayer.setTrack(currentPlaylist.getSelectionModel().getSelectedIndex());
                audioPlayer.play();
            }
        });
        currentPlaylist.setCellFactory(param -> new ListCell<>() {
            @Override
            protected void updateItem(Track track, boolean b) {
                super.updateItem(track, b);
                setStyle("-fx-background-color: #111;");
                if(!b && track != null) {
                    setText(track.toString());
                    if(getIndex() == param.getSelectionModel().getSelectedIndex()) {
                        setStyle("-fx-background-color: #068;");
                    }
                    if(audioPlayer.getCurrentTrack() != null && getItem().getId() == audioPlayer.getCurrentTrack().getId()) {
                        setStyle("-fx-background-color: #0bf;");
                    }
                }
                else {
                    setText("");
                }
                setStyle(getStyle() + "-fx-text-fill: #ddd; -fx-border-color: #333;");
            }
        });
    }

    private void addPlaylistUI(Playlist playlist) {
        List<Node> nodes = PlaylistUIFactory.createPlaylistUI(playlist, playlistCount++);
        nodes.get(1).setOnMouseClicked(mouseEvent -> {
            chosenPlaylist = playlist;
            playlistInfoButton.setDisable(false);
            addPlaylistButton.setDisable(false);
            currentPlaylist.getItems().clear();
            playlistName.setText("Загрузка...");
            boolean has = hasPlaylist(playlist);
            if(has) {
                addPlaylistButton.setText("Удалить");
                addPlaylistButton.setOnAction(actionEvent -> {
                    addPlaylistButton.setDisable(true);
                    Main.getHttpRequestsExecutor().execute(() -> {
                        try {
                            userAPI.deletePlaylist(getToken(), user.getId(), playlist.getId()).execute();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        update();
                        Platform.runLater(() -> {
                            addPlaylistButton.setDisable(false);
                        });
                    });
                });
            }
            else {
                addPlaylistButton.setText("Добавить");
                addPlaylistButton.setOnAction(actionEvent -> {
                    addPlaylistButton.setDisable(true);
                    Main.getHttpRequestsExecutor().execute(() -> {
                        try {
                            userAPI.addPlaylist(getToken(), user.getId(), playlist.getId()).execute();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        update();
                        Platform.runLater(() -> {
                            addPlaylistButton.setDisable(false);
                        });
                    });
                });
            }
            Main.getHttpRequestsExecutor().execute(() -> {
                setPlaylistTracks(playlist);
                Platform.runLater(() -> {
                    showPlaylist(playlist);
                });
            });
        });
        playlistPane.getChildren().add(nodes.get(0));
    }

    private boolean hasPlaylist(Playlist playlist) {
        for(Playlist plst : userPlaylists) {
            if(plst.getId() == playlist.getId()) {
                return true;
            }
        }
        return false;
    }

    private void clearPlaylistUI() {
        playlistPane.getChildren().clear();
        showPlaylist(null);
        playlistCount = 0;
        playlistInfoButton.setDisable(true);
        addPlaylistButton.setDisable(true);
        playlistPaneLabel.setText("");
    }

    private String secondToString(int s) {
        return s / 60 + ":" + (s % 60 < 10 ? "0" : "") + s % 60;
    }
}
