package ru.nsu.ccfit.mvcentertainment.communify.frontend.Controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.Genre;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.Playlist;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.GUI.Main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class PlaylistCreateController {
    @FXML private ImageView playlistCover;
    @FXML private TextField playlistName;
    @FXML private ComboBox<String> playlistGenre;
    @FXML private Button playlistCoverButton;
    @FXML private TextArea playlistDescription;
    @FXML private Button playlistSubmit;

    private File newCover = null;

    public void init(Stage stage) {
        Playlist playlist = new Playlist();
        playlist.setTracks(new ArrayList<>());
        for(Genre genre : Genre.values()) {
            playlistGenre.getItems().add(genre.name());
        }

        playlistCoverButton.setOnAction(actionEvent -> {
            newCover = PlaylistInfoController.getFileByDialogWindow();
            if(newCover != null) {
                playlistCover.setImage(new Image(newCover.toURI().toString()));
            }
        });

        playlistSubmit.setOnAction(actionEvent -> Main.getHttpRequestsExecutor().execute(() -> {
            try {
                playlistSubmit.setDisable(true);
                playlist.setName(playlistName.getText());
                playlist.setDescription(playlistDescription.getText());
                playlist.setGenre(playlistGenre.getValue());
                Playlist info = Playlist.playlistAPI.createPlaylist(MainController.getToken(), playlist).execute().body();
                playlist.setId(info.getId());
                Platform.runLater(() -> {
                    PlaylistInfoController.uploadTracks(null, playlist);
                    MainController.getMainController().update();
                    Window window = stage.getScene().getWindow();
                    window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(newCover != null) {
                PlaylistInfoController.uploadCover(newCover, playlist);
            }
        }));
    }
}
