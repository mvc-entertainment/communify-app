package ru.nsu.ccfit.mvcentertainment.communify.frontend.Controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import lombok.Getter;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.Genre;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.Playlist;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.GUI.Main;

import java.io.File;
import java.io.IOException;

public class PlaylistInfoController {
    @FXML private ImageView playlistCover;
    @FXML private TextField playlistName;
    @FXML private ChoiceBox<String> playlistGenre;
    @FXML private Button playlistCoverButton;
    @FXML private TextArea playlistDescription;
    @FXML private Button playlistSubmit;
    @FXML private Button playlistTracks;

    private File newCover = null;
    @Getter private Stage tracksEdit = null;

    public void init(Playlist playlist) {
        boolean isEditable = playlist.getOwner().getId() == MainController.getMainController().getUser().getId() &&
                (MainController.getMainController().getAudioPlayer().getPlaylist() == null ||
                        playlist.getId() != MainController.getMainController().getAudioPlayer().getPlaylist().getId());
        playlistName.setEditable(isEditable);
        playlistGenre.setDisable(!isEditable);
        playlistCoverButton.setDisable(!isEditable);
        playlistDescription.setEditable(isEditable);
        playlistSubmit.setDisable(!isEditable);
        playlistTracks.setDisable(!isEditable);

        Main.getHttpRequestsExecutor().execute(() -> {
            Image image = new Image(playlist.getImageCover());
            Platform.runLater(() -> {
                playlistCover.setImage(image);
            });
        });
        playlistName.setText(playlist.getName());
        for(Genre genre : Genre.values()) {
            playlistGenre.getItems().add(genre.name());
        }
        playlistGenre.setValue(playlist.getGenre());
        Main.getHttpRequestsExecutor().execute(() -> {
            try {
                Playlist info = MainController.getMainController().getPlaylistAPI().
                        getPlaylist(MainController.getToken(), playlist.getId()).execute().body();
                Platform.runLater(() -> {
                    playlistDescription.setText(info.getDescription());
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        playlistTracks.setOnAction(actionEvent -> {
            tracksEdit = uploadTracks(playlistTracks, playlist);
        });
        playlistCoverButton.setOnAction(actionEvent -> {
            newCover = getFileByDialogWindow();
            if(newCover != null) {
                playlistCover.setImage(new Image(newCover.toURI().toString()));
            }
        });
        playlistSubmit.setOnAction(actionEvent -> {
            playlistSubmit.setDisable(true);
            Main.getHttpRequestsExecutor().execute(() -> {
                    if (newCover != null) {
                        uploadCover(newCover, playlist);
                    }
                    try {
                        Playlist newInfo = new Playlist();
                        newInfo.setName(playlistName.getText());
                        newInfo.setDescription(playlistDescription.getText());
                        newInfo.setGenre(playlistGenre.getValue());
                        Playlist.playlistAPI.updatePlaylistInfo(MainController.getToken(), playlist.getId(), newInfo).execute();
                        Platform.runLater(() -> {
                            playlistSubmit.setDisable(false);
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            );
        });
    }

    public static File getFileByDialogWindow() {
        FileChooser mp3Chooser = new FileChooser();
        mp3Chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );
        mp3Chooser.setTitle("Выберите изображение");
        return mp3Chooser.showOpenDialog(new Stage());
    }

    public static void uploadCover(File cover, Playlist playlist) {
        Main.getHttpRequestsExecutor().execute(() -> {
            try {
                RequestBody coverFile = RequestBody.create(MediaType.parse("application/octet-stream"), cover);
                Playlist.playlistAPI.setPlaylistCover(MainController.getToken(), playlist.getId(), coverFile).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public static Stage uploadTracks(Button playlistTracks, Playlist playlist) {
        try {
            if(playlistTracks != null) {
                playlistTracks.setDisable(true);
            }
            Stage stage = new Stage();
            FXMLLoader loader = new FXMLLoader(PlaylistInfoController.class.getClassLoader().getResource("tracksEdit.fxml"));
            stage.setScene(new Scene(loader.load()));
            ((TracksEditController)loader.getController()).init(playlist);

            stage.setOnCloseRequest(windowEvent -> {
                if(playlistTracks != null) {
                    playlistTracks.setDisable(false);
                }
                ((TracksEditController)loader.getController()).getTrackUploads().forEach(Stage::close);
            });
            stage.setResizable(false);
            stage.setTitle("Изменение плейлиста");
            stage.show();
            return stage;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
