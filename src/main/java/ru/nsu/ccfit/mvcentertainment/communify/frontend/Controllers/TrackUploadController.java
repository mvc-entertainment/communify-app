package ru.nsu.ccfit.mvcentertainment.communify.frontend.Controllers;

import com.google.gson.JsonElement;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.Playlist;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.Track;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.GUI.Main;

import java.io.File;
import java.io.IOException;

public class TrackUploadController {
    @FXML private TextField trackName;
    @FXML private TextField author;
    @FXML private Button fileChooser;
    @FXML private Button submit;

    private File trackFile = null;

    public void init(Playlist playlist, Stage stage, TracksEditController tracksEditController) {
        fileChooser.setOnAction(actionEvent -> {
            FileChooser mp3Chooser = new FileChooser();
            mp3Chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("MP3", "*.mp3"));
            mp3Chooser.setTitle("Выберите .mp3 файл");
            trackFile = mp3Chooser.showOpenDialog(new Stage());
        });

        submit.setOnAction(actionEvent -> {
            if(trackName.getText().equals("")) {
                Main.error("Укажите название трека");
                return;
            }
            if(author.getText().equals("")) {
                Main.error("Укажите автора трека");
                return;
            }
            if(trackFile == null) {
                Main.error("Выберите файл трека");
                return;
            }
            Main.getHttpRequestsExecutor().execute(()-> {
                submit.setDisable(true);
                Track track = new Track(trackName.getText(), author.getText(), null, 0, "");
                RequestBody trackBody = RequestBody.create(MediaType.parse("multipart/form-data"), trackFile);
                MultipartBody.Part trackPart = MultipartBody.Part.createFormData("audioFile", "track", trackBody);
                try {
                    Playlist.playlistAPI.uploadTrackToPlaylists(MainController.getToken(),
                            playlist.getId(), track, trackPart).execute();
                    Platform.runLater(() -> {
                        tracksEditController.update();
                        Window window = stage.getScene().getWindow();
                        window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        });
    }
}
