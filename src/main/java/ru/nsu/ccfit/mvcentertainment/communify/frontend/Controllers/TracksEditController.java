package ru.nsu.ccfit.mvcentertainment.communify.frontend.Controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lombok.Getter;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.Playlist;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.Track;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.GUI.Main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TracksEditController {
    @FXML private ListView<Track> tracks;
    @FXML private Button upload;
    @FXML private Button delete;

    private Playlist playlist;
    @Getter private List<Stage> trackUploads = new ArrayList<>();

    public void init(Playlist playlist) {
        this.playlist = playlist;
        tracks.getItems().addAll(playlist.getTracks());
        tracks.setOnMouseClicked(mouseEvent -> {
            delete.setDisable(tracks.getSelectionModel().isEmpty());
        });

        upload.setOnAction(actionEvent -> {
            try {
                Stage stage = new Stage();
                FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("trackUpload.fxml"));
                stage.setScene(new Scene(loader.load()));
                ((TrackUploadController)loader.getController()).init(playlist, stage, this);

                stage.setResizable(false);
                stage.initModality(Modality.WINDOW_MODAL);
                stage.setTitle("Загрузка трека в плейлист");
                stage.show();
                trackUploads.add(stage);
                stage.setOnCloseRequest(windowEvent -> {
                    trackUploads.remove(stage);
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        delete.setOnAction(actionEvent -> {
            delete.setDisable(true);
            Main.getHttpRequestsExecutor().execute(() -> {
                try {
                    Playlist.playlistAPI.deleteTrackFromPlaylist(MainController.getToken(), playlist.getId(),
                            tracks.getSelectionModel().getSelectedItem().getId()).execute();
                    MainController.getMainController().update();
                    Platform.runLater(() -> {
                        update();
                        delete.setDisable(false);
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        });
    }

    public void update() {
        MainController.getMainController().updatePlaylist(playlist, true, () -> {
            tracks.getItems().clear();
            tracks.getItems().addAll(playlist.getTracks());
        });
    }
}
