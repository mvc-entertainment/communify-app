package ru.nsu.ccfit.mvcentertainment.communify.frontend.Controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.User;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.GUI.Main;

import java.io.File;
import java.io.IOException;

public class UserInfoController {
    @FXML private ImageView userImage;
    @FXML private TextField userNameField;
    @FXML private TextArea userBioField;
    @FXML private Button profileImageButton;
    @FXML private Button userSubmit;

    private File newImage = null;

    public void init(User user) {
        boolean isEditable = user.getId() == MainController.getMainController().getUser().getId();
        Main.getHttpRequestsExecutor().execute(() -> {
            try {
                User info = MainController.getMainController().getUserAPI().
                        getUser(MainController.getToken(), user.getId()).execute().body();
                Platform.runLater(() -> {
                    userNameField.setText(info.getName());
                    userBioField.setText(info.getBio());
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        Main.getHttpRequestsExecutor().execute(() -> {
            try {
                Image image = new Image(MainController.getMainController().getUserAPI().
                        getUserIcon(MainController.getToken(), user.getId()).execute().body().byteStream());
                Platform.runLater(() -> {
                    userImage.setImage(image);
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (NullPointerException ignored) {

            }
        });

        profileImageButton.setVisible(isEditable);
        userSubmit.setVisible(isEditable);
        userBioField.setEditable(isEditable);

        profileImageButton.setOnAction(actionEvent -> {
            newImage = PlaylistInfoController.getFileByDialogWindow();
            if(newImage != null) {
                userImage.setImage(new Image(newImage.toURI().toString()));
            }
        });

        userSubmit.setOnAction(actionEvent -> {
            userSubmit.setDisable(true);
            Main.getHttpRequestsExecutor().execute(() -> {
                    if (newImage != null) {
                        uploadImage(newImage, user);
                    }
                    try {
                        User info = new User();
                        info.setBio(userBioField.getText());
                        MainController.getMainController().getUserAPI().updateUser(MainController.getToken(), user.getId(), info).execute();
                        Platform.runLater(() -> {
                            userSubmit.setDisable(false);
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            );
        });
    }

    public static void uploadImage(File image, User user) {
        Main.getHttpRequestsExecutor().execute(() -> {
            try {
                RequestBody coverFile = RequestBody.create(MediaType.parse("application/octet-stream"), image);
                MainController.getMainController().getUserAPI().setUserIcon(MainController.getToken(), user.getId(), coverFile).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
