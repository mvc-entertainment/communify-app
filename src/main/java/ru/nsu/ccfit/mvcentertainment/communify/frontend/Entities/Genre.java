package ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities;

public enum Genre {
    ROCK,
    FUNK,
    METAL,
    JAZZ,
    FOLK,
}