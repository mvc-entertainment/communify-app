package ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities;

import javafx.scene.image.Image;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.API.PlaylistAPI;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.GUI.Main;

import java.util.List;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class Playlist {
    public static final PlaylistAPI playlistAPI;

    static {
        playlistAPI = Main.getRetrofit().create(PlaylistAPI.class);
    }

    private String name;
    private String genre;
    private String description;
    private User owner;
    private List<Track> tracks;
    private String imageCover;
    private long id;

    @Override
    public String toString() {
        return owner.getName() + " - " + name;
    }
}
