package ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class PlaylistFilter {
    private String name;
}
