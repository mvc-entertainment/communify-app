package ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class Track {
    private String name;
    private String author;
    private String media;
    private long id;
    private String description;

    @Override
    public String toString() {
        return author + " - " + name;
    }
}
