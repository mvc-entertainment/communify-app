package ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class TracksDTO {
    private List<Track> content;
}
