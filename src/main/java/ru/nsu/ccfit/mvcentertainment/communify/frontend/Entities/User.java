package ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter @Getter
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private String name;
    private String password;
    private String bio;
    private long id;
}
