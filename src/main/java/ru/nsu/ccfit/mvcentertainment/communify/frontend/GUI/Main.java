package ru.nsu.ccfit.mvcentertainment.communify.frontend.GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lombok.Getter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Controllers.AuthorizationController;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Controllers.MainController;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.JavaFXAudioPlayer;

import java.io.*;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Main extends Application {
    @Getter private static String token;
    @Getter private static Properties configuration;
    @Getter private static Retrofit retrofit;
    @Getter private static ExecutorService httpRequestsExecutor;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        configuration = new Properties();
        configuration.load(getClass().getClassLoader().getResource("config.properties").openStream());

        retrofit = new Retrofit.Builder()
                .baseUrl(Main.getConfiguration().getProperty("host.url"))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        httpRequestsExecutor = Executors.newFixedThreadPool(4);

        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("main.fxml"));
        Scene scene = new Scene(loader.load());
        MainController mainController = loader.getController();

        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.setTitle("Communify");
        primaryStage.setOnCloseRequest(windowEvent -> {
            System.exit(0);
        });
        primaryStage.show();

        checkAuthorization();

        mainController.init(new JavaFXAudioPlayer());
    }

    private void checkAuthorization() throws IOException {
        try {
            FileInputStream tokenFile = new FileInputStream("token");
            int c;
            StringBuilder tokenBuilder = new StringBuilder();
            while((c = tokenFile.read()) != -1) {
                tokenBuilder.append((char)c);
            }
            token = tokenBuilder.toString();
            tokenFile.close();
        }
        catch (FileNotFoundException fnfe) {
            createAuthorizationWindow();
        }
    }

    public static void createAuthorizationWindow() throws IOException {
        Stage authorization = new Stage();
        authorization.initModality(Modality.APPLICATION_MODAL);
        FXMLLoader loader = new FXMLLoader(Main.class.getClassLoader().getResource("authorization.fxml"));
        Scene scene = new Scene(loader.load());
        ((AuthorizationController)loader.getController()).init(authorization);
        authorization.setScene(scene);
        authorization.setTitle("Вход в Communify");
        authorization.setResizable(false);
        authorization.setOnCloseRequest(windowEvent -> {
            System.exit(0);
        });
        authorization.show();
    }

    public static void setToken(String newToken) {
        token = newToken;
        File tokenFile = new File("token");
        tokenFile.delete();
        if(token == null) {
            return;
        }
        try {
            FileOutputStream tokenStream = new FileOutputStream(tokenFile);
            for(char c : token.toCharArray()) {
                tokenStream.write(c);
            }
            tokenStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void error(String errorText) {
        Alert alert = new Alert(Alert.AlertType.ERROR, errorText);
        alert.show();
    }
}