package ru.nsu.ccfit.mvcentertainment.communify.frontend.GUI;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Controllers.MainController;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.Playlist;

import java.util.Arrays;
import java.util.List;


public class PlaylistUIFactory {
    public static final int PANE_WIDTH = 122;
    public static final int PANE_HEIGHT = 155;
    public static final int IMG_WIDTH = 110;
    public static final int IMG_HEIGHT = 110;
    public static final int BIAS = 10;

    public static List<Node> createPlaylistUI(Playlist playlist, int index) {
        Pane pane = new Pane();
        pane.setPrefWidth(122);
        pane.setPrefHeight(155);
        pane.setStyle(
                "-fx-background-color: #333;" +
                "-fx-background-radius: 5;"
        );
        pane.setLayoutX(BIAS * (index % 4 + 1) + PANE_WIDTH * (index % 4));
        pane.setLayoutY((double)(BIAS * (index / 4 + 1) + PANE_HEIGHT * (index / 4)));
        pane.setOnMouseEntered(mouseEvent -> {
            pane.setEffect(new DropShadow(10, Color.WHITE));
        });
        pane.setOnMouseExited(mouseEvent -> {
            pane.setEffect(null);
        });

        Pane cover = new Pane();
        cover.setPrefWidth(IMG_WIDTH);
        cover.setPrefHeight(IMG_HEIGHT);
        cover.setLayoutX(6);
        cover.setLayoutY(6);

        Label name = new Label(playlist.getName());
        name.setAlignment(Pos.CENTER);
        name.setPrefWidth(IMG_WIDTH);
        name.setLayoutX(6);
        name.setLayoutY(118);
        name.setTextFill(Color.WHITE);

        Label author = new Label(playlist.getOwner().getName());
        author.setAlignment(Pos.CENTER);
        author.setPrefWidth(IMG_WIDTH);
        author.setLayoutX(6);
        author.setLayoutY(135);
        author.setTextFill(Color.WHITE);
        MainController.setUserLabel(author);
        author.setOnMouseClicked(mouseEvent -> {
            MainController.getMainController().setChosenUser(playlist.getOwner());
            MainController.getMainController().getSearchField().setText("");
            Main.getHttpRequestsExecutor().execute(() -> {
                MainController.getMainController().update();
            });
        });

        pane.getChildren().add(cover);
        pane.getChildren().add(name);
        pane.getChildren().add(author);

        Main.getHttpRequestsExecutor().execute(() -> {
            Image image = new Image(playlist.getImageCover());
            if(!image.isError()) {
                Platform.runLater(() -> {
                    ImageView imageView = new ImageView(image);
                    imageView.setFitWidth(IMG_WIDTH);
                    imageView.setFitHeight(IMG_HEIGHT);
                    imageView.setEffect(new DropShadow());
                    imageView.setX(6);
                    imageView.setY(6);
                    imageView.setOnMouseClicked(cover.getOnMouseClicked());
                    pane.getChildren().remove(cover);
                    pane.getChildren().add(imageView);
                });
            }
        });
        return Arrays.asList(pane, cover);
    }
}
