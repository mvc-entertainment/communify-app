package ru.nsu.ccfit.mvcentertainment.communify.frontend;

import javafx.beans.value.ChangeListener;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.Playlist;
import ru.nsu.ccfit.mvcentertainment.communify.frontend.Entities.Track;

import java.util.ArrayList;
import java.util.List;

public class JavaFXAudioPlayer implements AudioPlayer {
    private static final double TIME_TO_CHANGE_TO_PREVIOUS = 5;

    private MediaPlayer mediaPlayer = null;
    private Playlist playlist = null;
    private double volume = 1;
    private int currentTrack = -1;
    private boolean playing = false;

    private List<ChangeListener<Duration>> timeListeners = new ArrayList<>();
    private List<Runnable> onTrackStart = new ArrayList<>();
    private List<Runnable> onPlaylistChange = new ArrayList<>();

    public JavaFXAudioPlayer() {

    }

    @Override
    public void play() {
        if(mediaPlayer != null) {
            mediaPlayer.play();
            playing = true;
        }
    }

    @Override
    public void pause() {
        if(mediaPlayer != null) {
            mediaPlayer.pause();
            playing = false;
        }
    }

    @Override
    public void seek(double duration) {
        if(mediaPlayer != null) {
            mediaPlayer.seek(new Duration(duration * mediaPlayer.getTotalDuration().toMillis()));
        }
    }

    @Override
    public double getCurrentTime() {
        if(mediaPlayer == null) {
            return 0;
        }
        return mediaPlayer.getCurrentTime().toMillis() / mediaPlayer.getTotalDuration().toMillis();
    }

    @Override
    public double getCurrentSeconds() {
        if(mediaPlayer == null) {
            return 0;
        }
        return mediaPlayer.getCurrentTime().toSeconds();
    }

    @Override
    public double getTotalSeconds() {
        if(mediaPlayer == null) {
            return 0;
        }
        return mediaPlayer.getTotalDuration().toSeconds();
    }

    @Override
    public double getVolume() {
        return volume;
    }

    @Override
    public Track getCurrentTrack() {
        if(currentTrack == -1) {
            return null;
        }
        return playlist.getTracks().get(currentTrack);
    }

    @Override
    public Playlist getPlaylist() {
        return playlist;
    }

    @Override
    public int getTrack() {
        return currentTrack;
    }

    @Override
    public void setVolume(double volume) {
        if(mediaPlayer != null) {
            mediaPlayer.setVolume(volume);
        }
        this.volume = volume;
    }

    @Override
    public void setPlaylist(Playlist playlist) {
        if(this.playlist != playlist) {
            if(this.playlist != null && playlist != null && this.playlist.getId() != playlist.getId()) {
                currentTrack = -1;
            }
            this.playlist = playlist;
            for(Runnable runnable : onPlaylistChange) {
                runnable.run();
            }
        }
    }

    @Override
    public void addTimeListener(ChangeListener<Duration> listener) {
        if(mediaPlayer != null) {
            mediaPlayer.currentTimeProperty().addListener(listener);
        }
        timeListeners.add(listener);
    }

    @Override
    public void addOnTrackStart(Runnable runnable) {
        onTrackStart.add(runnable);
    }

    @Override
    public void addOnPlaylistChange(Runnable runnable) {
        onPlaylistChange.add(runnable);
    }

    @Override
    public void setTrack(int index) {
        if(index >= 0 && index < playlist.getTracks().size() && index != currentTrack) {
            currentTrack = index;
            if(mediaPlayer != null) {
                mediaPlayer.stop();
            }
            mediaPlayer = new MediaPlayer(getMedia(playlist.getTracks().get(index)));
            mediaPlayer.setVolume(volume);
            mediaPlayer.setOnEndOfMedia(this::nextTrack);
            mediaPlayer.setOnPlaying(() -> {
                for(Runnable runnable : onTrackStart) {
                    runnable.run();
                }
            });
            if(!playing) {
                mediaPlayer.stop();
            }
            else {
                mediaPlayer.play();
            }
            for (ChangeListener<Duration> listener : timeListeners) {
                mediaPlayer.currentTimeProperty().addListener(listener);
            }
        }
    }

    @Override
    public void nextTrack() {
        setTrack(currentTrack + 1);
    }

    @Override
    public void previousTrack() {
        if(getCurrentSeconds() < TIME_TO_CHANGE_TO_PREVIOUS) {
            setTrack(currentTrack - 1);
        }
        else {
            seek(0);
        }
    }

    @Override
    public boolean isPlaying() {
        return playing;
    }

    private Media getMedia(Track track) {
        return new Media(track.getMedia());
    }
}
